#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plainText[PASS_LEN];
    char hashText[33];
};

typedef struct entry entry;

int cmpFunc(const void *e1, const void *e2);
int bSearchFunc(const void *e1, const void *e2);

// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *fptr;
    fptr = fopen(filename, "r");
    if(!fptr){
        fprintf(stderr, "Can't open dictionary for reading.\n");
        perror(NULL);
        exit(1);
    }
    char line[PASS_LEN+1];
    entry *array = NULL;
    int i = 0;
    while(fgets(line, PASS_LEN+1, fptr)){
        char *nl = strchr(line, '\n');
        if(nl){
            *nl = '\0';
        }
        array = realloc(array, sizeof(entry)*(i+1));
        strcpy(array[i].plainText, line);
        char *hash = md5(array[i].plainText, strlen(line));
        strcpy(array[i].hashText, hash);
        free(hash);
        i++;
    }
    *size = i;
    fclose(fptr);
    return array;
}


int main(int argc, char *argv[])
{

    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures
    int size = 0;
    struct entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, size, sizeof(entry), cmpFunc);

    // TODO
    // Open the hash file for reading.
    FILE *hptr;
    hptr = fopen(argv[1], "r");
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    // Need only one loop. (Yay!)
    char line[33];
    while(fgets(line, 33, hptr)){
        char *nl = strchr(line, '\n');
        if(nl){
            *nl = '\0';
        }
        entry *found = bsearch(&line, dict, size, sizeof(entry), bSearchFunc);
        if(found){
            printf("Found! Plaintext: %s Hashtext: %s\n", found->plainText, found->hashText);
        }
    }
    fclose(hptr);
    free(dict);
}

int cmpFunc(const void *e1, const void *e2){
    return strcmp(((entry*)e1)->hashText, ((entry*)e2)->hashText);
}

int bSearchFunc(const void *e1, const void *e2){
    return strcmp((char*)e1, ((entry*)e2)->hashText);
}